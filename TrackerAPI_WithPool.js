var express = require('express');
var multer = require('multer')
var upload = multer({ dest: 'uploads/' })
var mysql = require("mysql");
var bodyParser = require('body-parser')
var http = require('http');
var path = require('path');
var fs = require("fs");

var app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
    extended: true
}));
app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});
app.set('title', 'Tracker API');

var pool = mysql.createPool({
    host: "localhost",
    database: "tracker",
    user: "dotNetApp",
    password: "dotNetApp"
});

app.get('/', function (req, res) {

    res.send({ "NodeJS WebAPI Test": "OK" });

});

app.post('/UploadImage', upload.single('file'), function (req, res, next) {
    console.log("Getting image file: " + req.file.originalname);
    fs.rename('uploads/' + req.file.filename, 'uploads/' + req.file.filename + "_" + req.file.originalname);
    res.json({ filename: req.file.filename + "_" + req.file.originalname, mimetype: req.file.mimetype, originalname: req.file.originalname });
})


app.get('/Image', function (req, res) {
    res.sendFile(path.resolve('uploads/' + req.query["filename"]));
});

app.get('/Certificate', function (req, res) {
    pool.getConnection(function (err, connection) {
        connection.query('CALL getCertificates(' + req.query["uid"] + ')', function (err, rows) {
            if (err) throw err;
            res.set('Content-Type', 'application/json');
            res.send(JSON.stringify(rows));
            connection.release();
        });
    });
});

app.post('/Certificate', function (req, res) {

    var expDate = null;
    if (req.body.ed)
        expDate = "'" + req.body.ed.replace(/T/, ' ').replace(/\..+/, '') + "'";

    pool.getConnection(function (err, connection) {

        connection.query("CALL addNewCertificate('" + req.body.uid + "','" + req.body.p + "','" + req.body.s + "','" + req.body.cn + "','" + req.body.id.replace(/T/, ' ').replace(/\..+/, '') + "'," + expDate + ",'" + req.body.ib + "')", function (err, rows) {

            if (err) {
                res.send('{"Status": 0, "Message": "Sorry!"}');
                console.log(err);
            }
            else {

                if (req.body.filename && req.body.origFilename && req.body.mimetype && rows[0][0].TrackerId > 0) {
                    connection.query("CALL addNewCertificateFile('" + req.body.uid + "','" + req.body.filename + "','" + req.body.origFilename + "','" + req.body.mimetype + "','" + rows[0][0].TrackerId + "')", function (err1, rows1) {

                        if (err1)
                            console.log(err1);

                        res.json({ Status: 1, trackerId: rows[0][0].TrackerId, filename: req.body.filename });


                    });
                }
                else {
                    res.json({ Status: 1, trackerId: rows[0][0].TrackerId });
                }
            }

            connection.release();

        });
    });
});

http.createServer(app).listen(88);
console.log("Node JS Express is listening to port 88 ...");
